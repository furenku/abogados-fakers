import React from 'react';

import {Doughnut} from 'react-chartjs-2';

const AreaVisitsView = ({data}) => (
    <div className="AreaVisits Chart">
        <Doughnut data={data} options={{legend:{display:true}}}/>
    </div>
)

export default AreaVisitsView;