import React, { Component } from 'react';


import AreaVisitsView from './AreaVisitsView';


import sourceData from '../../datasets/visitAreas.json'



const itemsDisplayedNum = 3;

const colorBaseR = 16;
const colorVarianceR = 20;
const colorBaseG = 108;
const colorVarianceG = 30;
const colorBaseB = 157;
const colorVarianceB = 10;

const hoverValueIncrease = 20;

class AreaVisits extends Component {

    state = {
        data: null
    }

    componentDidMount() {

        let data = {};
        data.labels = []
        data.labels = []
        data.datasets = [{}]
        
        
        let currentDataset = data.datasets[0]
        currentDataset.data = [];
        currentDataset.backgroundColor = []
        currentDataset.hoverBackgroundColor = []

        for( let i = 0; i < itemsDisplayedNum; i++ ) {

            let areaVisitsData = sourceData.shift()
            
            currentDataset.data.push( areaVisitsData.count )
            
              

            let colorR = colorBaseR + (colorVarianceR*i);
            let colorG = colorBaseG + (colorVarianceG*i);
            let colorB = colorBaseB + (colorVarianceB*i);
            
            currentDataset.backgroundColor.push( `rgba(${colorR},${colorG},${colorB})` );
            currentDataset.hoverBackgroundColor.push( `rgba(${colorR+hoverValueIncrease},${colorG+hoverValueIncrease},${colorB+hoverValueIncrease})` );


            // random colors:
            // currentDataset.backgroundColor.push('#'+Math.floor(Math.random()*16777215).toString(16));
            // currentDataset.hoverBackgroundColor.push('#'+Math.floor(Math.random()*16777215).toString(16));

            data.labels.push( areaVisitsData.areaLabel );


        }

        this.setState({ data })

    }

    render() {

        return <AreaVisitsView data={this.state.data}/>

    }

}

export default AreaVisits;