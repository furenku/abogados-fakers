import React, { Component } from 'react';


import MonthVisitsView from './MonthVisitsView';


import sourceData from '../../datasets/visitHistoryMonthly.json'



const chartColor = '#106c9d'


class MonthVisits extends Component {

    state = {
        data: null
    }

    componentDidMount() {

        let data = {};
        data.labels = []
        data.datasets = [{}]


        let currentDataset = data.datasets[0]
        currentDataset.data = [];
        currentDataset.backgroundColor = []
        currentDataset.hoverBackgroundColor = []


        const monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

        for( let month in sourceData ) {
        
            data.labels.push( monthNames[month] );
            currentDataset.data.push( sourceData[month].length )
            
            currentDataset.backgroundColor.push(chartColor);
            currentDataset.hoverBackgroundColor.push(chartColor);


            // // random colors
            // currentDataset.backgroundColor.push('#'+Math.floor(Math.random()*16777215).toString(16));
            // currentDataset.hoverBackgroundColor.push('#'+Math.floor(Math.random()*16777215).toString(16));

        }

        this.setState({ data })

    }

    render() {

        return <MonthVisitsView data={this.state.data}/>

    }

}

export default MonthVisits;