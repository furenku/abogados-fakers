import React from 'react';

import {Bar} from 'react-chartjs-2';

const MonthVisitsView = ({data}) => (
    <div className="MonthVisits Chart">
        <Bar data={data} options={{legend:{display:false}}}/>
    </div>
)

export default MonthVisitsView;