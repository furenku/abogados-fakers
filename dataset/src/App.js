import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import MonthVisits from './components/MonthVisits/MonthVisits';
import AreaVisits from './components/AreaVisits/AreaVisits';




class App extends Component {
  render() {
    return (
      <div className="App">
        
        <MonthVisits/>
        <AreaVisits/>

      </div>
    );
  }
}

export default App;
