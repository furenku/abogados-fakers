const fs = require('fs');

const areasNum = 50;
const maxRecordsNum = 1000;

let areas = []


for( let i = 0; i<areasNum; i++ ) {
    
    let area = {
        id: i,
        areaLabel: "Nombre Completo de Área de Especialidad "+(i+1),
        count: Math.ceil( Math.random() * maxRecordsNum )
    }
    
    areas.push( area );
}



areas = areas.sort((a,b)=>{ return b.count - a.count });

let data = JSON.stringify( areas );

fs.writeFileSync('visitAreas.json', data);  
