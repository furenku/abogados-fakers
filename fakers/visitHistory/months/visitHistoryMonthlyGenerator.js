const fs = require('fs');


const recordsNum = 10000;
const year = 2018;



let randomVisits = []
let orderedVisits = {}

for( let i = 0; i<recordsNum; i++ ) {
    
    let month = Math.floor(Math.random()*12);
    let day = Math.floor(Math.random()*31);
    let hour = Math.floor(Math.random()*24);
    let minutes = Math.floor(Math.random()*60);
    let seconds = Math.floor(Math.random()*60);
    
    randomVisits.push(
        new Date( year, month, day, hour, minutes, seconds)
    );

}


randomVisits.map( visit => {
    
    const month = visit.getMonth();

    if( ! orderedVisits[month] ) {
        orderedVisits[month] = []
    }

    orderedVisits[month].push( visit.getTime() )
    
});


for( month in orderedVisits ) {
    orderedVisits[month] = orderedVisits[month].sort((a,b) => a-b)
}


let data = JSON.stringify( orderedVisits );

fs.writeFileSync('visitHistoryMonthly.json', data);  
