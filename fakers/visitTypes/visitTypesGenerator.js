const fs = require('fs');

const types = [ "Correo", "Teléfono", "Perfil" ]

const maxRecordsNum = 1000;

let visitTypes = []


for( let i = 0; i<types.length; i++ ) {
    
    let type = {
        id: i,
        typeLabel: types[i],
        count: Math.ceil( Math.random() * maxRecordsNum )
    }
    
    visitTypes.push( type );

}



visitTypes = visitTypes.sort((a,b)=>{ return b.count - a.count });

let data = JSON.stringify( visitTypes );

fs.writeFileSync('visitTypes.json', data);  
